//
//  ListViewController.swift
//  Wedding Wazir
//
//  Created by Asmita Bharti on 24/06/16.
//  Copyright © 2016 gopinathjew. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {

    @IBAction func enquirybtn(sender: AnyObject) {
        enqyirycontainer.hidden = false
        reservecontainer.hidden = true
        cancelcontainer.hidden = true
        
    }
    
    @IBAction func cancelbtn(sender: AnyObject) {
        reservecontainer.hidden = true
        enqyirycontainer.hidden = true
        cancelcontainer.hidden = false

    }
    @IBAction func reservedbtn(sender: AnyObject) {
        enqyirycontainer.hidden = true
        cancelcontainer.hidden = true
        reservecontainer.hidden = false

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reservecontainer.hidden = true
        cancelcontainer.hidden = true
        // Do any additional setup after loading the view.
    }
    @IBOutlet var cancelcontainer: UIView!
    @IBOutlet var reservecontainer: UIView!
    @IBOutlet var enqyirycontainer: UIView!

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
