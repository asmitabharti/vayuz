//
//  LeadViewController.swift
//  Wedding Wazir
//
//  Created by Asmita Bharti on 27/06/16.
//  Copyright © 2016 gopinathjew. All rights reserved.
//

import UIKit

class LeadViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var names = ["abc"]
    var occasions = ["wedding"]
    var date = ["23/465"]


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBOutlet var tableview: UITableView!

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableview.dequeueReusableCellWithIdentifier("cell" , forIndexPath: indexPath)as! Leadcustomcell
        
        cell.namelabel.text = names[indexPath.row]
        cell.occasion.text = occasions[indexPath.row]
        cell.datelabel.text = date[indexPath.row]
        
        return cell

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
