//
//  venuecustomCell.swift
//  Wedding Wazir
//
//  Created by Asmita Bharti on 27/06/16.
//  Copyright © 2016 gopinathjew. All rights reserved.
//

import UIKit

class venuecustomCell: UITableViewCell {

    @IBOutlet var locationimg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet var foodlabel: UILabel!
    @IBOutlet var peoplelabel: UILabel!
    @IBOutlet var outdoorlabel: UILabel!
    @IBOutlet var label1: UILabel!
    @IBOutlet var foodimg: UIImageView!
    @IBOutlet var peopleimg: UIImageView!
    @IBOutlet var outdoorimg: UIImageView!

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
