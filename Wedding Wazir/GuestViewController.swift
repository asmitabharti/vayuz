//
//  GuestViewController.swift
//  Wedding Wazir
//
//  Created by Asmita Bharti on 24/06/16.
//  Copyright © 2016 gopinathjew. All rights reserved.
//

import UIKit

class GuestViewController: UIViewController {

    @IBOutlet var segmentcontrol: UISegmentedControl!
    
    @IBOutlet var managevenue: UIView!
    
@IBOutlet weak var profileimage: UIImageView!
    
    @IBOutlet var about: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        profileimage.layer.cornerRadius = profileimage.frame.size.width / 2;
    
       profileimage.clipsToBounds = true;
        managevenue.hidden = true
        
        
        
        // Do any additional setup after loading the view.
    }
    @IBAction func segment(sender: UISegmentedControl) {
        switch segmentcontrol.selectedSegmentIndex
        {
        case 0:
            about.hidden = false
            managevenue.hidden = true
        case 1:
            about.hidden = true
            managevenue.hidden = false
        default:
            break;
        }
    }
    
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
    
        /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
