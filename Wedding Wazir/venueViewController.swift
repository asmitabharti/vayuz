//
//  venueViewController.swift
//  Wedding Wazir
//
//  Created by Asmita Bharti on 24/06/16.
//  Copyright © 2016 gopinathjew. All rights reserved.
//

import UIKit

class venueViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {
    

    @IBOutlet var tableview: UITableView!
    @IBAction func availablevenue(sender: AnyObject) {
        
        
    }
    
    var locationlabel = ["Maple Exotica","Noida"]
    var outdoor = ["Outdoor","Outdoor"]
    var people = ["200-300","400-500"]
    var food = ["Non-veg","Veg"]
    var images1 = [UIImage(named: "venuem"),UIImage(named: "spprofilea")]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func adaptivePresentationStyleForPresentationController(
        controller: UIPresentationController!) -> UIModalPresentationStyle {
            return .None
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationlabel.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableview .dequeueReusableCellWithIdentifier("venuecell",forIndexPath: indexPath)as! venuecustomCell
        cell.locationimg.image = images1[indexPath.row]
      
        

        cell.label1.text = locationlabel[indexPath.row]
        cell.foodlabel.text = food[indexPath.row]
        cell.peoplelabel.text = people[indexPath.row]
        cell.outdoorlabel.text = outdoor[indexPath.row]

        return cell

        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
